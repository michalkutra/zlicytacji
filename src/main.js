// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import moment from 'moment';
import axios from '../node_modules/axios/index';
import App from './App.vue';
import router from './router/index';
import store from './store/index';

Vue.config.productionTip = false;
window.axios = axios;

moment.locale('pl');

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
