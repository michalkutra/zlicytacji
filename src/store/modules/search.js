// import types from '../mutation-types';

// initial state
const state = {
  searchParams: {
    type: {
      name: null,
      urlname: null,
    },
    city: {
      name: null,
      urlname: null,
    },
    page: 1,
  },
  auctions: {},
};

// getters
const getters = {
};

// actions
const actions = {
};

// mutations
const mutations = {
};

export default {
  state,
  getters,
  actions,
  mutations,
};
