// import types from '../mutation-types';

// initial state
const state = {
  auctionId: null,
  auctionParams: {

  },
};

// getters
const getters = {
};

// actions
const actions = {
};

// mutations
const mutations = {
};

export default {
  state,
  getters,
  actions,
  mutations,
};
