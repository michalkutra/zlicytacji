import moment from 'moment';

export const diff = (date) => moment(date).diff(moment());
export const fromNow = (date) => moment(date).fromNow();
