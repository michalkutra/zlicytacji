import config from '../config/config';

export default {
  API_ENDPOINT_GLOBAL_ACUTIONS_COUNT: `${config.API_DOMAIN}/stats/global-auctions-count.json`,
  API_ENDPOINT_POPULAR_QUERIES: `${config.API_DOMAIN}/popular-queries.json`,
  API_ENDPOINT_CITIES: `${config.API_DOMAIN}/cities.json`,
  API_ENDPOINT_PROPERTY_TYPES: `${config.API_DOMAIN}/property-types.json`,
  API_ENDPOINT_LATEST_PROPERTIES: `${config.API_DOMAIN}/latest-properties.json`,
  API_ENDPOINT_SEARCH_LISTING: `${config.API_DOMAIN}/search-results.json`,
  API_ENDPOINT_AUCTION: `${config.API_DOMAIN}/auction.json`,
};
