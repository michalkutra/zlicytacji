import Vue from 'vue';
import Router from 'vue-router';
import Homepage from '../components/Homepage.vue';
import Contact from '../components/Contact.vue';
import SearchListing from '../components/SearchListing.vue';
import Auction from '../components/Auction.vue';
import Archive from '../components/Archive.vue';
import Terms from '../components/Terms.vue';
import BuyPremium from '../components/BuyPremium.vue';
import YourAccount from '../components/YourAccount.vue';
import Login from '../components/Login.vue';
import Register from '../components/Register.vue';

Vue.use(Router);

const routes = [
  {
    name: 'route_homepage',
    path: '/',
    component: Homepage,
  },
  {
    name: 'route_archive',
    path: '/archiwum/:page',
    component: Archive,
  },
  {
    name: 'route_search',
    path: '/:type/:city/:page?',
    component: SearchListing,
  },
  {
    name: 'route_auction',
    path: '/licytacja/:type/:city/:id',
    component: Auction,
  },
  {
    name: 'route_contact',
    path: '/kontakt',
    component: Contact,
  },
  {
    name: 'route_terms',
    path: '/regulamin',
    component: Terms,
  },
  {
    name: 'route_buy_premium',
    path: '/kup-premium',
    component: BuyPremium,
  },
  {
    name: 'route_your_account',
    path: '/twoje-konto',
    component: YourAccount,
  },
  {
    name: 'route_login',
    path: '/logowanie',
    component: Login,
  }, {
    name: 'route_register',
    path: '/rejestracja',
    component: Register,
  },
];

export default new Router({
  mode: 'history',
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});
