export default {
  DOMAIN: 'zlicytacji.com.pl',
  API_DOMAIN: 'http://local.api.zlicytacji',
  BRAND: {
    TITLE: 'Nieruchomości z licytacji komorniczych',
    SUBTITLE: '%count% ofert w bazie',
  },
  SEARCH_RESULTS: {
    RESULTS_PER_PAGE: 7,
  },
};
