module.exports = {
  deploy : {
    production : {
      user : "deploy",
      host : "88.99.187.52",
      ref  : "origin/master",
      repo : "git@bitbucket.org:michalkutra/zlicytacji.git",
      path : "/home/www/zlicytacji.com.pl/",
      "post-deploy" : "yarn install --prod --silent --ignore-optional --ignore-platform --no-progress --no-emoji --frozen-lockfile && npm run build || :"
    }
  }
}
